__title__ = "CLI Todo App"
__version__ = "1.0.0"
__author__ = "Sheikh Hafijul Ali"
__author_email__ = "hafijul.dev@gmail.com"
__description__ = "CLI application for saving todos."
__copyright__ = "Copyright 2023-2024 Sheikh Hafijul Ali"
__all__ = [
    "todo"
]